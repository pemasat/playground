import triangle from './index';

describe("Pascal's triangle", () => {

  test("Can be initialized", () => {
    expect(typeof triangle === "function").toBe(true);
    expect(Array.isArray(triangle())).toBe(true);
    expect(Array.isArray(triangle())).toBe(true);
    expect(Array.isArray(triangle({resultCount: 2}))).toBe(true);
    expect(Array.isArray(triangle({resultCount: 2, inicializedState: [[1]]}))).toBe(true);
  });

  test("Give correct result", () => {
    expect(getArrayCheckSum(triangle({
      resultCount: 1,
    }))).toBe(getArrayCheckSum([[1]]));
    expect(getArrayCheckSum(triangle({
      resultCount: 2,
    }))).toBe(getArrayCheckSum([[1], [1,1]]));
    expect(getArrayCheckSum(triangle({
      inicializedState: [[2]],
      resultCount: 1,
    }))).toBe(getArrayCheckSum([[2]]));
    expect(getArrayCheckSum(triangle({
      inicializedState: [[2]],
      resultCount: 2,
    }))).toBe(getArrayCheckSum([[2], [2,2]]));
  });

});

const getArrayCheckSum = (array) => JSON.stringify(array);
