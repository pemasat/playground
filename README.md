# Playground

Small example of code

## Pascal's triangle
inspirated from [wikipedia](https://en.wikipedia.org/wiki/Pascal%27s_triangle), source is in folder `triangle`, you can call it with params:
- `resultCount` number of rows of pyramid, default is `5`
- `inicializedState` top of pyramid, default is `[[1]]`

like
```
triangle({
  resultCount: 2,
  inicializedState: [[2, 3]],
});
```

result in default call without params is
```
   1  
  1 1
 1 2 1
1 3 3 1
```
